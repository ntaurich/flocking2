#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Agent.generated.h"

UCLASS()
class FLOCKING_API AAgent : public AActor
{
	GENERATED_BODY()
	
public:	
	AAgent();
	void Init( UStaticMeshComponent *mesh, int id );

	UPROPERTY(EditAnywhere)
	class UStaticMeshComponent * Mesh;

	UPROPERTY(EditAnywhere)
	FVector Velocity;

	FVector GetLoc();
	int idnum;

protected:
	virtual void BeginPlay() override;
	FVector location;

public:	
	virtual void Tick(float DeltaTime) override;
};