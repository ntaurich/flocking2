#include "FlockingManager.h"
#include "Agent.h"

#define AGENT_COUNT 10

void UFlockingManager::Init( UWorld *world, UStaticMeshComponent *mesh ) {
    UE_LOG(LogTemp, Warning, TEXT("Manager initialized"));
    
    World = world;
    float incr = (PI * 2.f) / AGENT_COUNT;
    for( int i = 0; i < AGENT_COUNT; i++ ) {
        if( World != nullptr ) {
            FRotator rotation = FRotator();

            //lay boids in a ring
            FVector location = FVector::ZeroVector; 
            location.X = FMath::Sin( incr * i ) * 600.f;
            location.Y = FMath::Cos( incr * i ) * 600.f;

            AAgent * agent = World->SpawnActor<AAgent>( location, rotation );
            agent->Init( mesh, i ); //init with mesh and id number
            Agents.Add( agent );
        }
    }

    initialized = true;
}

/*
void UFlockingManager::Flock() {
    UE_LOG(LogTemp, Warning, TEXT("Starting Flock") );
    for(auto& Boid: Agents){
    //for(int32 i =0 ; i< Agents.Num(); ++i){
        //AAgent* Boid = Agents[i];
        UE_LOG(LogTemp, Warning, TEXT("Before Rule One") );
        //FVector R1 =  RuleOne(Boid);
        // FVector R2 = FVector(0.f);//RuleTwo(Boid);
        // FVector R3 = FVector(0.f); //RuleThree(Boid);
        // FVector R4 = FVector(0.f);//Bound(Boid);

        FVector NewVelocity = Boid->Velocity;
        UE_LOG(LogTemp, Warning, TEXT("After Rule One") );
        // R1.Normalize();
        // R2.Normalize();
        // R3.Normalize();
        // NewVelocity = NewVelocity + (((R1)+ (R2) +(R3))/60) + R4 ;
        // //FMath::Clamp(NewVelocity,1, 2);
        // float max = 10;
        // float min = 5;
        // const float speed = FMath::Clamp(NewVelocity.Size(), min, max );
        // NewVelocity.Normalize();
        // NewVelocity = NewVelocity * speed ;
        float vx =NewVelocity.X;
        float vy =NewVelocity.Y;
        float vz = NewVelocity.Z;
        //Boid->Velocity.Set(vx,vy,vz);

    }
    UE_LOG(LogTemp, Warning, TEXT("Loop Complete") );

}
*/

void UFlockingManager::Flock() {
    UE_LOG(LogTemp, Warning, TEXT("Starting Flock") );
    for(int32 i =0 ; i< Agents.Num(); ++i){
        AAgent* Boid = Agents[i];
        UE_LOG(LogTemp, Warning, TEXT("Before Rule One") );
        FVector R1 = RuleOne(Boid);
        FVector R2 = RuleTwo(Boid);
        FVector R3 = RuleThree(Boid);
        FVector R4 = Bound(Boid);

        FVector NewVelocity = Boid->Velocity;
        UE_LOG(LogTemp, Warning, TEXT("After Rule One") );
        R1.Normalize();
        R2.Normalize();
        R3.Normalize();
        NewVelocity = NewVelocity + (((R1)+ (R2) +(R3))/60) + R4 ;
        float max = 10;
        float min = 5;
        const float speed = FMath::Clamp(NewVelocity.Size(), min, max );
        NewVelocity.Normalize();
        NewVelocity = NewVelocity * speed ;
        float vx =NewVelocity.X;
        float vy =NewVelocity.Y;
        float vz = NewVelocity.Z;
        Boid->Velocity.Set(vx,vy,vz);

    }
    UE_LOG(LogTemp, Warning, TEXT("Loop Complete") );

}
/*
FVector UFlockingManager::RuleOne(AAgent* CurrBoid){ //towards center of other boids
    FVector Center = FVector::ZeroVector; 
    //for(int32 j =0 ; j< Agents.Num(); ++j){
    for(auto& Boid: Agents){
        //if(IsValid(Agents[i])){
    //        if (Agents[j]->idnum != CurrBoid->idnum){
                UE_LOG(LogTemp, Warning, TEXT("Agents Equal") );
                //Center = Center + Agents[i]->GetActorLocation();
    //        }
        //}
        // else{
        //     UE_LOG(LogTemp, Warning, TEXT("NOT VALID") );
        // }
    }
    UE_LOG(LogTemp, Warning, TEXT("Finished Inner Loop") );
    // Center = Center / (AGENT_COUNT-1);
    return Center;
   // return ((Center - CurrBoid->GetActorLocation())/200);

}
*/


FVector UFlockingManager::RuleOne(AAgent* CurrBoid){ //towards center of other boids
    FVector Center = FVector::ZeroVector; 
    for(int32 j =0 ; j< Agents.Num(); ++j){
            if (Agents[j]->idnum != CurrBoid->idnum){
                UE_LOG(LogTemp, Warning, TEXT("Agents Equal") );
                Center = Center + Agents[j]->GetActorLocation();
            }
    }
    UE_LOG(LogTemp, Warning, TEXT("Finished Inner Loop") );
    Center = Center / (AGENT_COUNT-1);
   return ((Center - CurrBoid->GetActorLocation())/200);

}


FVector UFlockingManager::RuleTwo(AAgent* CurrBoid){ //separation
    FVector C = FVector(0.f);
    for(int i =0 ; i< AGENT_COUNT; i++){
        if (Agents[i] != CurrBoid ){
            FVector loc1 = Agents[i]->GetActorLocation();
            FVector loc2 = CurrBoid->GetActorLocation();
            float dist = FVector::Dist(loc1, loc2);
            dist = abs(dist);
            //float dist = VSize(Agents[i]->GetActorLocation()-CurrBoid->GetActorLocation());
            if(dist < 300){
                C = C - (loc1-loc2);
                UE_LOG(LogTemp, Warning, TEXT("Distance: %f"), dist);
            }
        }

    }
    return (C/ (AGENT_COUNT-1));

}

FVector UFlockingManager::RuleThree(AAgent* CurrBoid){ //Align
    FVector Center = FVector(0.f);
    for(int i =0 ; i< AGENT_COUNT; i++){
        if (Agents[i] != CurrBoid){
            Center = Center + Agents[i]->Velocity;
        }
    }

    Center = Center / (AGENT_COUNT-1);
    return ((Center - (CurrBoid->Velocity))/8);
}

FVector UFlockingManager::Bound (AAgent* CurrBoid){
    int Xmin =-2500;
    int Xmax =2500;
    int Ymin =-2500;
    int Ymax =2500;
    int Zmin =-2500;
    int Zmax =2500;
    FVector v = FVector(0.f);

    if (CurrBoid->GetActorLocation().X<Xmin){
        v.X =10;
    }
    else if (CurrBoid->GetActorLocation().X>Xmax){
        v.X = -10;
    }
    if (CurrBoid->GetActorLocation().Y<Ymin){
        v.Y =10;
    }
    else if (CurrBoid->GetActorLocation().Y>Ymax){
        v.Y = -10;
    }    
    if (CurrBoid->GetActorLocation().Z<Zmin){
        v.Z =10;
    }
    else if (CurrBoid->GetActorLocation().Z>Zmax){
        v.Z = -10;
    }
    return v;


}

/*
	PROCEDURE tend_to_place(Boid b)
		Vector place

		RETURN (place - b.position) / 100
	END PROCEDURE
    */
