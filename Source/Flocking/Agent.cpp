#include "Agent.h"

AAgent::AAgent(){
	PrimaryActorTick.bCanEverTick = true;
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("AgentMesh"));	
	RootComponent = Mesh;
	//FVector randNum = FMath::VRand();
	Velocity = FMath::VRand();
	Velocity = Velocity.GetSafeNormal();
	Velocity = Velocity * FMath::RandRange(5,10);
	//FMath::RandRange
	//Velocity = FVector(5); //initialize to zero
}

void AAgent::BeginPlay(){
	Super::BeginPlay();
}

void AAgent::Init( UStaticMeshComponent *mesh, int id ) {
	UE_LOG(LogTemp, Warning, TEXT("Agent initialized.") );
	Mesh->SetStaticMesh( mesh->GetStaticMesh() );
	location = GetActorLocation();
	idnum = id;
}

void AAgent::Tick(float DeltaTime){
	Super::Tick(DeltaTime);
	UE_LOG(LogTemp, Warning, TEXT("Before Tick") );
	FRotator angle = Velocity.Rotation();
	FRotator angle2 = FRotator(angle.Pitch-90,angle.Yaw, angle.Roll);
	SetActorRotation(angle2, ETeleportType::None);
	const FVector loc = GetActorLocation();
	SetActorLocation( loc + Velocity );
	UE_LOG(LogTemp, Warning, TEXT("After Tick") );
	//AAgent::SetActorRotation(Velocity.Rotation().,ETeleportType::None);
	
}

FVector AAgent::GetLoc(){
	return location;
}